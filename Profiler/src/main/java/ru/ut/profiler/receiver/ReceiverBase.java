package ru.ut.profiler.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import ru.ut.profiler.commons.event.EventArgs;
import ru.ut.profiler.commons.event.Handler;

/**
 * User: utegental
 * Date: 03.02.14
 * Time: 17:19
 */
public abstract class ReceiverBase<T extends Handler<? extends EventArgs>> extends BroadcastReceiver {

    private final T handler = initHandler();
    private boolean isRegistered;


    protected abstract T initHandler();

    protected abstract String[] getActionNames();


    public T getHandler() {
        return handler;
    }

    public final synchronized void start(Context context) {
        if (!isRegistered) {
            for (String action : getActionNames()) {
                context.registerReceiver(this, new IntentFilter(action));
            }

            isRegistered = true;
        }
    }


    public void stop(Context context) {
        context.unregisterReceiver(this);
    }
}

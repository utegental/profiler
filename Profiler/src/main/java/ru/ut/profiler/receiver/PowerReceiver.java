package ru.ut.profiler.receiver;

import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.util.Log;
import ru.ut.profiler.model.enums.Charging;
import ru.ut.profiler.receiver.args.PowerEventArgs;

/**
 * User: utegental
 * Date: 03.01.14
 * Time: 13:22
 */
public class PowerReceiver extends ReceiverBase<PowerReceiver.Handler> {
    private static String[] actions = new String[]{Intent.ACTION_BATTERY_CHANGED,
            Intent.ACTION_BATTERY_LOW,
            Intent.ACTION_BATTERY_OKAY,
            Intent.ACTION_POWER_CONNECTED,
            Intent.ACTION_POWER_DISCONNECTED
    };
    int previousLevel = -1;

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            this.proceed(context, intent);
        } catch (Exception exception) {
            Log.e("Battery", "", exception);
        }
    }

    void proceed(Context context, Intent intent) {
        int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);


        int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);

        Charging charging = Charging.Unknown;

        switch (status) {

            case BatteryManager.BATTERY_STATUS_CHARGING:
            case BatteryManager.BATTERY_STATUS_FULL:
                if (chargePlug == BatteryManager.BATTERY_PLUGGED_USB) {
                    charging = Charging.USB;
                } else if (chargePlug == BatteryManager.BATTERY_PLUGGED_AC) {
                    charging = Charging.AC;
                }
                break;
            default:
                charging = Charging.Unplugged;
                break;
        }

        float batteryPct = level;
        Log.i("Battery", "PowerReceiver");
        if (level != -1 && scale != -1) {
            batteryPct = level * 100 / (float) scale;

            String message = String.format("Battery level %s. Charging: %s", batteryPct, charging);
            //Log.i("Battery", message);
        }

        /*Intent i = new Intent(CustomIntent.STATE);
        i.putExtra(StateManager.BATTERY_LEVEL, level);
        i.putExtra(StateManager.CHARGING_STATE, charging.value);
        context.sendBroadcast(i);
*/
        if(level != -1){
            previousLevel = level;
        } else {
            level = previousLevel;
        }
        String message = String.format("Battery level %s. Charging: %s", level, charging);
        Log.i("Battery", message);
        getHandler().onChanged(level, charging);
    }

    @Override
    protected Handler initHandler() {
        return new Handler();
    }

    @Override
    protected String[] getActionNames() {
        return actions;
    }


    public static class Handler extends ru.ut.profiler.commons.event.Handler<PowerEventArgs> {
        private void onChanged(int level, Charging charging) {
            this.invoke(new PowerEventArgs(level, charging));
        }
    }
}

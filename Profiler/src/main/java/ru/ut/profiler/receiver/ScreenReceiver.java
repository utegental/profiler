package ru.ut.profiler.receiver;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import ru.ut.profiler.model.enums.ScreenState;
import ru.ut.profiler.receiver.args.ScreenEventArgs;

/**
 * User: utegental
 * Date: 07.01.14
 * Time: 1:22
 */
public class ScreenReceiver extends ReceiverBase<ScreenReceiver.Handler> {

    private static String[] actions = new String[]{
            Intent.ACTION_SCREEN_ON,
            Intent.ACTION_SCREEN_OFF
    };

    @Override
    public void onReceive(Context context, Intent intent) {
        ScreenState screenState = ScreenState.Unknown;

        switch (intent.getAction()) {
            case Intent.ACTION_SCREEN_ON:
                screenState = ScreenState.On;
                break;
            case Intent.ACTION_SCREEN_OFF:
                screenState = ScreenState.Off;
                break;
        }


        Log.d("Battery", String.format("Screen state changed to %s", screenState));
        /*Intent i = new Intent(CustomIntent.STATE);
        i.putExtra(StateManager.SCREEN_STATE, screenState.value);
        context.sendBroadcast(i);*/

        getHandler().onChanged(screenState);
    }

    @Override
    protected Handler initHandler() {
        return new Handler();
    }

    @Override
    protected String[] getActionNames() {
        return actions;
    }


    public static class Handler extends ru.ut.profiler.commons.event.Handler<ScreenEventArgs> {
        private void onChanged(ScreenState screenState) {
            this.invoke(new ScreenEventArgs(screenState));
        }
    }
}

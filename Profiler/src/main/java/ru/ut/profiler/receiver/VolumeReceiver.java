package ru.ut.profiler.receiver;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import ru.ut.profiler.receiver.args.VolumeEventArgs;

/**
 * User: utegental
 * Date: 02.02.14
 * Time: 16:23
 */
public class VolumeReceiver extends ReceiverBase<VolumeReceiver.Handler> {

    private static String[] actions = new String[]{"android.media.VOLUME_CHANGED_ACTION"};

    private int prevRingerValue = -1;
    private int prevMediaVolumeValue = -1;

    @Override
    public void onReceive(Context context, Intent intent) {

        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        int ringerValue = audioManager.getStreamVolume(AudioManager.STREAM_RING);
        int mediaVolumeValue = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
       /* boolean value = false;

        switch (audioManager.getRingerMode()) {
            case AudioManager.RINGER_MODE_SILENT:
            case AudioManager.RINGER_MODE_VIBRATE:
                value = false;
                break;
            case AudioManager.RINGER_MODE_NORMAL:
                value = true;
                break;
        }*/

        if(prevMediaVolumeValue != -1 && prevRingerValue != -1) {
            getHandler().onVolumeChanged(ringerValue, mediaVolumeValue);
        }

        prevMediaVolumeValue = mediaVolumeValue;
        prevRingerValue = ringerValue;

        //showDialog(context);
        /*Intent i = new Intent(CustomIntent.STATE);
        i.putExtra(StateManager.VOLUME_STATE, value);
        context.sendBroadcast(i);*/
    }

    @Override
    protected Handler initHandler() {
        return new Handler();
    }

    @Override
    protected String[] getActionNames() {
        return actions;
    }

    public static class Handler extends ru.ut.profiler.commons.event.Handler<VolumeEventArgs> {
        private void onVolumeChanged(int ringerValue, int mediaVolumeValue) {
            this.invoke(new VolumeEventArgs(ringerValue, mediaVolumeValue));
        }
    }
}

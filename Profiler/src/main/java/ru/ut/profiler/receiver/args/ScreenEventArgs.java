package ru.ut.profiler.receiver.args;

import ru.ut.profiler.commons.event.EventArgs;
import ru.ut.profiler.model.enums.ScreenState;

/**
 * User: utegental
 * Date: 03.02.14
 * Time: 17:11
 */
public class ScreenEventArgs extends EventArgs {
    private final ScreenState screenState;

    public ScreenEventArgs(ScreenState screenState) {
        this.screenState = screenState;
    }

    public ScreenState getScreenState() {
        return screenState;
    }
}

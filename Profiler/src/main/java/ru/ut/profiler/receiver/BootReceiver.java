package ru.ut.profiler.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import ru.ut.profiler.service.MainService;

/**
 * User: utegental
 * Date: 03.01.14
 * Time: 22:19
 */
public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent intent1 = new Intent(context, MainService.class);
        context.startService(intent1);
    }
}

package ru.ut.profiler.receiver.args;

import ru.ut.profiler.commons.event.EventArgs;
import ru.ut.profiler.model.enums.Charging;

/**
 * User: utegental
 * Date: 03.02.14
 * Time: 16:55
 */
public class PowerEventArgs extends EventArgs {
    private int level;
    private Charging charging;

    public PowerEventArgs(int level, Charging charging) {
        this.level = level;
        this.charging = charging;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Charging getCharging() {
        return charging;
    }

    public void setCharging(Charging charging) {
        this.charging = charging;
    }
}

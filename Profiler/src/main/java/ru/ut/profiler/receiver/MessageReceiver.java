package ru.ut.profiler.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import ru.ut.profiler.model.DecisionMakingCenter;
import ru.ut.profiler.model.Parameters;
import ru.ut.profiler.model.controller.VolumeController;
import ru.ut.profiler.model.enums.Charging;
import ru.ut.profiler.model.enums.ScreenState;

/**
 * User: utegental
 * Date: 03.01.14
 * Time: 16:49
 */
public class MessageReceiver extends BroadcastReceiver {

    private DecisionMakingCenter center = new DecisionMakingCenter();
    private Parameters previousParameters;
    private VolumeController volumeController;

    public MessageReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {

        int batteryLevel = intent.getIntExtra(StateManager.BATTERY_LEVEL, -1);
        short chargingValue = intent.getShortExtra(StateManager.CHARGING_STATE, (short) -1);
        short screenStateValue = intent.getShortExtra(StateManager.SCREEN_STATE, (short) -1);

        Charging charging = chargingValue != -1 ? Charging.parse(chargingValue) : Charging.Unknown;
        ScreenState screenState = screenStateValue != -1 ? ScreenState.parse(screenStateValue) : ScreenState.Unknown;


        Parameters parameters = previousParameters == null ? new Parameters() : new Parameters(previousParameters);

        parameters.setContext(context);

        if (batteryLevel != -1) {
            parameters.setBatteryLevel(batteryLevel);
        }

        if (charging != Charging.Unknown) {
            parameters.setCharging(charging);
        }

        if (screenState != ScreenState.Unknown) {
            parameters.setScreenState(screenState);
        }

        Parameters.fillCurrentTime(parameters);

        previousParameters = parameters;
        center.reflect(parameters);
    }

    public VolumeController getVolumeController() {
        return volumeController;
    }

    public void setVolumeController(VolumeController volumeController) {
        this.volumeController = volumeController;
    }
}

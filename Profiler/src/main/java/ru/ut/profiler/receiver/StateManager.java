package ru.ut.profiler.receiver;

/**
 * User: utegental
 * Date: 06.01.14
 * Time: 12:18
 */
public class StateManager {
    public final static String BATTERY_LEVEL = "level";
    public final static String CHARGING_STATE = "battery.state";
    public final static String SCREEN_STATE = "screen.state";
    public final static String VOLUME_STATE = "volume.state";
    public final static String VOLUME_VALUE = "volume.value";


    public final static int BATTERY_LEVEL_DEFAULT = -1;
}

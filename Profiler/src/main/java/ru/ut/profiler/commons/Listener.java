package ru.ut.profiler.commons;


/**
 * User: utegental
 * Date: 01.02.14
 * Time: 19:20
 */
public interface Listener<T> {

    void added(T task);

    void removed(T task);
}

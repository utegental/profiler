package ru.ut.profiler.commons.event;

/**
 * User: utegental
 * Date: 03.02.14
 * Time: 16:40
 */
public interface Listener<T extends EventArgs> {
    void invoke(T args);
}

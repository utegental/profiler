package ru.ut.profiler.commons.event;

import java.util.LinkedList;
import java.util.List;

/**
 * User: utegental
 * Date: 03.02.14
 * Time: 16:43
 */
public abstract class Handler<T extends EventArgs> {

    private final List<Listener<T>> listeners = new LinkedList<>();
    private final Object syncRoot = new Object();

    public void add(Listener<T> listener) {
        synchronized (syncRoot) {
            listeners.add(listener);
        }
    }

    public void remove(Listener<T> listener) {
        synchronized (syncRoot) {
            listeners.remove(listener);
        }
    }

    protected void invoke(T args) {
        synchronized (syncRoot) {
            for (Listener<T> listener : listeners) {
                listener.invoke(args);
            }
        }
    }
}

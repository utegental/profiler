package ru.ut.profiler.commons;


import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * User: utegental
 * Date: 01.02.14
 * Time: 21:49
 */
public class ObservableList<T> implements Iterable<T> {
    private final List<T> tasks = new LinkedList<>();
    private final Handler handler = new Handler();

    public void add(T item) {
        tasks.add(item);
        handler.added(item);
    }

    public void addAll(Collection<? extends T> c) {
        tasks.addAll(c);
        for (T item : c) {
            handler.added(item);
        }
    }

    public void remove(T item) {
        tasks.remove(item);
        handler.removed(item);
    }

    @Override
    public Iterator<T> iterator() {
        return tasks.iterator();
    }

    public Handler getHandler() {
        return handler;
    }

    public static class Handler<T> {
        private final List<Listener> listeners = new LinkedList<>();

        public void add(Listener listener) {
            listeners.add(listener);
        }

        public void remove(Listener listener) {
            listeners.remove(listener);
        }

        private void added(T item) {
            for (Listener listener : listeners) {
                listener.added(item);
            }
        }

        void removed(T item) {
            for (Listener listener : listeners) {
                listener.removed(item);
            }
        }
    }


}

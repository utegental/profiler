package ru.ut.profiler;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.*;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.widget.DrawerLayout;
import android.view.*;
import android.widget.TextView;
import ru.ut.profiler.model.enums.ServiceState;
import ru.ut.profiler.receiver.CustomIntent;
import ru.ut.profiler.receiver.ServiceManager;
import ru.ut.profiler.service.MainService;

public class MainActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            /*MainService.LocalBinder binder = (MainService.LocalBinder) service;*/
            //mService = binder.getService();
            //mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            //mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        Intent intent = new Intent(MainActivity.this, MainService.class);

        BroadcastReceiver serviceReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                short serviceStateValue = intent.getShortExtra(ServiceManager.SERVICE_STATE, (short) -1);
                ServiceState serviceState = serviceStateValue != -1 ? ServiceState.parse(serviceStateValue) : ServiceState.Unknown;
                switch (serviceState) {
                    case Stopped:
                        unbindService(mConnection);
                        break;
                    case Started:
                        bindService(intent, mConnection, Context.BIND_DEBUG_UNBIND);
                }
            }
        };

        ContextWrapper wrapper = new ContextWrapper(this.getBaseContext());
        wrapper.registerReceiver(serviceReceiver, new IntentFilter(CustomIntent.SERVICE));

        if (!Helper.isServiceStarted(this, MainService.class)) {
            startService(intent);
        }


        bindService(intent, mConnection, Context.BIND_DEBUG_UNBIND);


        //startService(new Intent(MainActivity.this, MainService.class));
                /*PowerReceiver receiver = new PowerReceiver();
        IntentFilter ifilterChanged = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        IntentFilter ifilterLow = new IntentFilter(Intent.ACTION_BATTERY_LOW);
        IntentFilter ifilterOkay = new IntentFilter(Intent.ACTION_BATTERY_OKAY);

        Intent batteryStatus = registerReceiver(receiver, ifilterChanged);

lok
        Intent batteryStatusLow = registerReceiver(receiver, ifilterLow);
        Intent batteryStatusOkay = registerReceiver(receiver, ifilterOkay);*/


    }


    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, DummyFragment.newInstance(position + 1))
                .commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle(mTitle);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A dummy fragment containing a simple view.
     */
    public static class DummyFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static DummyFragment newInstance(int sectionNumber) {
            DummyFragment fragment = new DummyFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public DummyFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            if (rootView != null) {
                TextView dummyTextView = (TextView) rootView.findViewById(R.id.section_label);
                if (dummyTextView != null) {
                    dummyTextView.setText(Integer.toString(getArguments().getInt(ARG_SECTION_NUMBER)));
                }
            }
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

}

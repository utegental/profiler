package ru.ut.profiler;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;

/**
 * User: utegental
 * Date: 08.01.14
 * Time: 9:13
 */
public class Helper {

    public static boolean isServiceStarted(Activity activity, Class<? extends Service> type) {
        boolean result = false;
        ActivityManager manager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (type.getName().equals(service.service.getClassName())) {
                result = true;
                break;
            }
        }
        return result;
    }
}

package ru.ut.profiler.model.sp.md;

import ru.ut.profiler.model.Task;
import ru.ut.profiler.model.action.ActionSwitchMobileData;
import ru.ut.profiler.model.sp.Conditions;

/**
 * User: utegental
 * Date: 02.03.14
 * Time: 22:23
 */
public class DisableLowBatteryTask extends Task {
    public DisableLowBatteryTask() {
        super(new ActionSwitchMobileData(false));

        getConditions().add(Conditions.lowBatteryCondition());
    }
}

package ru.ut.profiler.model;

/**
 * User: utegental
 * Date: 02.02.14
 * Time: 13:33
 */
public class TimeInterval {
    private final Time from;
    private final Time to;

    public TimeInterval(Time from, Time to) {
        this.from = from;
        this.to = to;
    }

    public Time getFrom() {
        return from;
    }

    public Time getTo() {
        return to;
    }
}

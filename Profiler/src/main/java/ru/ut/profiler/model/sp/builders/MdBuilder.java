package ru.ut.profiler.model.sp.builders;

import ru.ut.profiler.model.Task;
import ru.ut.profiler.model.sp.md.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * User: utegental
 * Date: 01.03.14
 * Time: 23:27
 */
public class MdBuilder extends BuilderBase {

    @Override
    public Collection<Task> createTasks() {
        BuilderParams params = getParams();
        return new ArrayList<>(Arrays.asList(new EnableDayTask(params.getDay()),
                new EnableEveningTask(params.getEvening()),
                new EnableMorningTask(params.getMorning()),
                new DisableNightTask(params.getNight()),
                new DisableMorningTask(params.getMorning()),
                new DisableEveningTask(params.getEvening()),
                new DisableDayTask(params.getDay()),
                new EnableNightTask(params.getNight()),
                new DisableLowBatteryTask(),
                new EnableChargingTask()));
    }
}

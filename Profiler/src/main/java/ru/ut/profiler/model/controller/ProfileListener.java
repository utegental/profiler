package ru.ut.profiler.model.controller;

import ru.ut.profiler.model.Profile;

/**
 * User: utegental
 * Date: 03.02.14
 * Time: 10:43
 */
public interface ProfileListener {

    void onActiveChanged(Profile profile);
}

package ru.ut.profiler.model.sp.builders;

import ru.ut.profiler.model.Task;
import ru.ut.profiler.model.sp.wifi.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * User: utegental
 * Date: 01.03.14
 * Time: 23:38
 */
public class WifiBuilder extends BuilderBase {

    @Override
    public Collection<Task> createTasks() {

        return new ArrayList<>(Arrays.asList(new DisableNightTask(getParams().getNight()),
                new DisableEveningTask(getParams().getEvening()),
                new DisableMorningTask(getParams().getMorning()),
                new DisableDayTask(getParams().getDay()),
                new EnableNightTask(getParams().getNight()),
                new EnableDayTask(getParams().getDay()),
                new EnableMorningTask(getParams().getMorning()),
                new EnableEveningTask(getParams().getEvening()),
                new DisableLowBatteryTask(),
                new EnableChargingTask()));
    }
}

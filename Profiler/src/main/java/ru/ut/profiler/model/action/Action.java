package ru.ut.profiler.model.action;

import android.content.Context;

/**
 * User: utegental
 * Date: 06.01.14
 * Time: 1:05
 */
public interface Action {
    void invoke(Context context);
}

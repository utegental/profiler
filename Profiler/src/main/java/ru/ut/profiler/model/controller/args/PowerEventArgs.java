package ru.ut.profiler.model.controller.args;

import ru.ut.profiler.commons.event.EventArgs;
import ru.ut.profiler.model.enums.Charging;

/**
 * User: utegental
 * Date: 03.02.14
 * Time: 16:50
 */
public class PowerEventArgs extends EventArgs {
    private final int level;
    private final Charging charging;

    public PowerEventArgs(int level, Charging charging) {
        this.level = level;
        this.charging = charging;
    }

    public int getLevel() {
        return level;
    }

    public Charging getCharging() {
        return charging;
    }
}

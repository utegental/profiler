package ru.ut.profiler.model.sp.wifi;

import ru.ut.profiler.model.action.ActionSwitchWifi;
import ru.ut.profiler.model.sp.ChargingAcBase;

/**
 * User: utegental
 * Date: 02.03.14
 * Time: 22:31
 */
public class EnableChargingTask extends ChargingAcBase {
    public EnableChargingTask() {
        super(new ActionSwitchWifi(true));
    }
}

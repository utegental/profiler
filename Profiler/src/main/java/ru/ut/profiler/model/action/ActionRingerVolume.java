package ru.ut.profiler.model.action;

import ru.ut.profiler.model.enums.RingerMode;

/**
 * User: utegental
 * Date: 03.03.14
 * Time: 13:46
 */
public class ActionRingerVolume extends ActionRingerMode {
    public ActionRingerVolume(RingerMode value) {
        super(value);
    }
}

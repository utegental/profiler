package ru.ut.profiler.model.sp.wifi;

import ru.ut.profiler.model.Task;
import ru.ut.profiler.model.TimeInterval;
import ru.ut.profiler.model.action.ActionSwitchWifi;
import ru.ut.profiler.model.condition.Condition;
import ru.ut.profiler.model.condition.ConditionBattery;
import ru.ut.profiler.model.condition.ConditionTimeInterval;
import ru.ut.profiler.model.enums.Charging;
import ru.ut.profiler.model.sp.Conditions;

import java.util.List;

/**
 * Created by Pavel on 06.04.2014.
 */
public class DisableMorningTask extends Task {
    public DisableMorningTask(TimeInterval morningInterval) {
        super(new ActionSwitchWifi(false));

        final List<Condition> conditions = this.getConditions();

        conditions.add(new ConditionTimeInterval(morningInterval));
        conditions.add(new ConditionBattery(Charging.Unplugged));
        conditions.add(Conditions.screenOffTimeoutCondition());
    }
}

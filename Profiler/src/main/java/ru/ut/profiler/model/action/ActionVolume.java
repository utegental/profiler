package ru.ut.profiler.model.action;

import android.content.Context;
import android.media.AudioManager;

/**
 * User: utegental
 * Date: 04.02.14
 * Time: 22:23
 */
public class ActionVolume implements Action {


    private final int ringValue;
    private final int mediaVolumeValue;

    public ActionVolume(int ringValue, int mediaVolumeValue) {
        /*if(ringValue > RING_VOLUME_MAX){
            throw new Exception("Ring volume value is too big");
        }*/

        this.ringValue = ringValue;
        this.mediaVolumeValue = mediaVolumeValue;
    }

    @Override
    public void invoke(Context context) {
        AudioManager manager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        int currentRingValue = manager.getStreamVolume(AudioManager.STREAM_RING);
        int currentMediaVolumeValue = manager.getStreamVolume(AudioManager.STREAM_MUSIC);

        if(currentRingValue != ringValue) {
            manager.setStreamVolume(AudioManager.STREAM_RING, ringValue, 0);
        }

        if(currentMediaVolumeValue != mediaVolumeValue){
            manager.setStreamVolume(AudioManager.STREAM_MUSIC, mediaVolumeValue, 0);
        }
    }
}

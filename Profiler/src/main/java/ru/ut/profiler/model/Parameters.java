package ru.ut.profiler.model;

import android.content.Context;
import org.joda.time.Duration;
import ru.ut.profiler.model.enums.Charging;
import ru.ut.profiler.model.enums.ScreenState;
import ru.ut.profiler.model.enums.ScreenStateChanged;

import java.util.Calendar;

/**
 * User: utegental
 * Date: 04.01.14
 * Time: 12:39
 */
public class Parameters {
    private Integer batteryLevel;
    private Charging charging;
    private ScreenState screenState;
    private ScreenStateChanged screenStateChanged = ScreenStateChanged.NotChanged;
    private Context context;
    private Duration screenStatePeriod;
    private Time time;

    public Parameters() {
    }

    public Parameters(Integer batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public Parameters(Integer batteryLevel, Charging charging) {
        this.batteryLevel = batteryLevel;
        this.charging = charging;
    }

    public Parameters(Parameters parameters) {
        this.batteryLevel = parameters.getBatteryLevel();
        this.charging = parameters.getCharging();
        this.context = parameters.getContext();
        this.screenState = parameters.getScreenState();
        this.screenStatePeriod = parameters.getScreenStatePeriod();
    }

    public static void fillCurrentTime(Parameters parameters) {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        parameters.setTime(new Time(hour, minute));
    }

    public Integer getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(Integer batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public Charging getCharging() {
        return charging;
    }

    public void setCharging(Charging charging) {
        this.charging = charging;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ScreenState getScreenState() {
        return screenState;
    }

    public void setScreenState(ScreenState screenState) {
        this.screenState = screenState;
    }

    public Duration getScreenStatePeriod() {
        return screenStatePeriod;
    }

    public void setScreenStatePeriod(Duration screenStatePeriod) {
        this.screenStatePeriod = screenStatePeriod;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public ScreenStateChanged getScreenStateChanged() {
        return screenStateChanged;
    }

    public void setScreenStateChanged(ScreenStateChanged screenStateChanged) {
        this.screenStateChanged = screenStateChanged;
    }
}

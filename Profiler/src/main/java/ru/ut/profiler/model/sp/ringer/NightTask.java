package ru.ut.profiler.model.sp.ringer;

import ru.ut.profiler.model.Task;
import ru.ut.profiler.model.TimeInterval;
import ru.ut.profiler.model.action.ActionRingerMode;
import ru.ut.profiler.model.condition.ConditionTimeInterval;
import ru.ut.profiler.model.enums.RingerMode;

/**
 * User: utegental
 * Date: 02.03.14
 * Time: 14:30
 */
public class NightTask extends Task {
    public NightTask(TimeInterval nightInterval) {
        super(new ActionRingerMode(RingerMode.Vibrating));
        getConditions().add(new ConditionTimeInterval(nightInterval));
    }
}

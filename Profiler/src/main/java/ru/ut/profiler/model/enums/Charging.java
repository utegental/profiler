package ru.ut.profiler.model.enums;

/**
 * User: utegental
 * Date: 04.01.14
 * Time: 12:18
 */
public enum Charging {
    Unknown((short) 0),
    DoesntMatter((short) 1),
    Unplugged((short) 2),
    USB((short) 2),
    AC((short) 3);

    public short value;

    Charging(short value) {
        this.value = value;
    }

    public static Charging parse(short value) {
        Charging result = Charging.Unknown;
        for (Charging charging : Charging.values()) {
            if (charging.value == value) {
                result = charging;
                break;
            }
        }
        return result;
    }
}

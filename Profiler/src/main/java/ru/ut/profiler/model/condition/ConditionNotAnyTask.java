package ru.ut.profiler.model.condition;

import ru.ut.profiler.model.ITask;
import ru.ut.profiler.model.Parameters;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Pavel on 06.04.2014.
 */
public class ConditionNotAnyTask implements Condition {

    private List<ITask> tasks = new LinkedList<>();

    public ConditionNotAnyTask(){}

    public void addTask(ITask task){
        tasks.add(task);
    }

    @Override
    public boolean check(Parameters parameters) {
        boolean result = true;

        for (ITask task : getTasks()){
            if(task.check(parameters)){
                result = false;
                break;
            }
        }

        return result;
    }

    public List<ITask> getTasks() {
        return tasks;
    }
}

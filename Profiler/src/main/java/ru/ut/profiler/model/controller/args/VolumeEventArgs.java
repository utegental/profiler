package ru.ut.profiler.model.controller.args;

import ru.ut.profiler.commons.event.EventArgs;

/**
 * User: utegental
 * Date: 04.02.14
 * Time: 9:22
 */
public class VolumeEventArgs extends EventArgs {
    private final int ringerValue;
    private final int mediaVolumeValue;

    public VolumeEventArgs(int ringerValue, int mediaVolumeValue){
        this.ringerValue = ringerValue;
        this.mediaVolumeValue = mediaVolumeValue;
    }

    public int getRingerValue() {
        return ringerValue;
    }

    public int getMediaVolumeValue() {
        return mediaVolumeValue;
    }
}

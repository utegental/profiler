package ru.ut.profiler.model.enums;

/**
 * User: utegental
 * Date: 02.02.14
 * Time: 12:11
 */
public enum Compare {
    Lower,
    Greater,
    Equal
}

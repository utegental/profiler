package ru.ut.profiler.model;

/**
 * User: utegental
 * Date: 03.02.14
 * Time: 10:44
 */
public class Profile {
    private final String name;

    public Profile(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

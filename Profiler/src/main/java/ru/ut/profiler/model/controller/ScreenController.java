package ru.ut.profiler.model.controller;

import ru.ut.profiler.model.controller.args.ScreenEventArgs;

/**
 * User: utegental
 * Date: 03.02.14
 * Time: 17:15
 */
public class ScreenController extends ControllerBase<ScreenEventArgs, ru.ut.profiler.receiver.args.ScreenEventArgs> {
    @Override
    protected ScreenEventArgs initArgs(ru.ut.profiler.receiver.args.ScreenEventArgs args) {
        return new ScreenEventArgs(args.getScreenState());
    }
}

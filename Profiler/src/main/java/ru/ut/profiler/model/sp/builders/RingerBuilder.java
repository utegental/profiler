package ru.ut.profiler.model.sp.builders;

import ru.ut.profiler.model.Task;
import ru.ut.profiler.model.sp.ringer.DayTask;
import ru.ut.profiler.model.sp.ringer.NightTask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * User: utegental
 * Date: 02.03.14
 * Time: 14:44
 */
public class RingerBuilder extends BuilderBase {

    @Override
    public Collection<Task> createTasks() {
        return new ArrayList<>(Arrays.asList(new NightTask(getParams().getNight()),
                new DayTask(getParams().getDay())));
    }
}

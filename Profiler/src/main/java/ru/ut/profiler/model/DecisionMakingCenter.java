package ru.ut.profiler.model;

import android.content.Context;
import android.os.PowerManager;
import android.util.Log;
import org.joda.time.Duration;
import org.joda.time.Instant;
import ru.ut.profiler.R;
import ru.ut.profiler.commons.Listener;
import ru.ut.profiler.model.action.ActionProfileSetter;
import ru.ut.profiler.model.action.ActionVolume;
import ru.ut.profiler.model.condition.*;
import ru.ut.profiler.model.controller.PowerController;
import ru.ut.profiler.model.controller.ProfileController;
import ru.ut.profiler.model.controller.ScreenController;
import ru.ut.profiler.model.controller.args.PowerEventArgs;
import ru.ut.profiler.model.controller.args.ScreenEventArgs;
import ru.ut.profiler.model.enums.ScreenState;
import ru.ut.profiler.model.enums.ScreenStateChanged;
import ru.ut.profiler.model.sp.builders.*;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * User: utegental
 * Date: 04.01.14
 * Time: 12:23
 */
public class DecisionMakingCenter {


    private final Time nightStart = new Time(23, 0);
    private final Time morningStart = new Time(7, 0);
    private final TimeInterval night = new TimeInterval(nightStart, morningStart);
    private final Time dayStart = new Time(9, 0);
    private final TimeInterval morning = new TimeInterval(morningStart, dayStart);
    private final Time eveningStart = new Time(20, 0);
    private final TimeInterval day = new TimeInterval(dayStart, eveningStart);
    private final TimeInterval evening = new TimeInterval(eveningStart, nightStart);


    private final Tasks tasks = new Tasks();
    private final ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(5);

    private ConditionNotAnyTask conditionNotAnyTask = new ConditionNotAnyTask();


    private Parameters previousParameters;
    private ProfileController profileController;
    private PowerController powerController;
    private ScreenController screenController;
    private AtomicReference<Instant> screenStateSwitchedInstant = new AtomicReference<>(Instant.now());
    private List<Duration> timeoutsScreenOn = new LinkedList<>();
    private List<Duration> timeoutsScreenOff = new LinkedList<>();

    public DecisionMakingCenter() {

        this.initHandler();
        //this.initCenter();
    }

    private void initHandler() {
        tasks.getHandler().add(new Listener<Task>() {
            @Override
            public void added(Task task) {
                taskAdded(task);
            }

            @Override
            public void removed(Task task) {
                taskRemoved(task);
            }
        });
    }



    public void setContext(Context context) {
        if (previousParameters == null) {
            previousParameters = new Parameters();
        }

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        ScreenState screenState = pm.isScreenOn() ? ScreenState.On : ScreenState.Off;



        previousParameters.setScreenState(screenState);
        previousParameters.setContext(context);
    }

    public void initCenter() {

        final WifiBuilder wifi = new WifiBuilder();
        final RingerBuilder ringer = new RingerBuilder();
        final MdBuilder md = new MdBuilder();
        final VolumeBuilder volume = new VolumeBuilder();

        final BuilderParams params = new BuilderParams();
        final Builder[] builders = new Builder[]{wifi, md, ringer, volume};

        params.setDay(day);
        params.setNight(night);
        params.setEvening(evening);
        params.setMorning(morning);

        Context context = previousParameters.getContext();

        String profileDayName = context.getString(R.string.profile_day_name);
        String profileNightName = context.getString(R.string.profile_night_name);
        String profileMorningName = context.getString(R.string.profile_morning_name);
        String profileEveningName = context.getString(R.string.profile_evening_name);

        Task setDay = new Task(new ActionProfileSetter(profileController, profileController.addProfile(new Profile(profileDayName))));
        setDay.getConditions().add(new ConditionTimeInterval(day));

        Task setNight = new Task(new ActionProfileSetter(profileController, profileController.addProfile(new Profile(profileNightName))));
        setNight.getConditions().add(new ConditionTimeInterval(night));

        Task setMorning = new Task(new ActionProfileSetter(profileController, profileController.addProfile(new Profile(profileMorningName))));
        setMorning.getConditions().add(new ConditionTimeInterval(morning));

        Task setEvening = new Task(new ActionProfileSetter(profileController, profileController.addProfile(new Profile(profileEveningName))));
        setEvening.getConditions().add(new ConditionTimeInterval(evening));

        Tasks tasks = this.getTasks();

        for (Builder builder : builders) {
            builder.setParams(params);
            tasks.addAll(builder.createTasks());
        }

        tasks.add(setDay);
        tasks.add(setEvening);
        tasks.add(setMorning);
        tasks.add(setNight);

        startInitial();
    }

    private void startInitial(){
        for (Duration duration : timeoutsScreenOff) {
            executorService.schedule(createTask(), duration.getMillis(), TimeUnit.MILLISECONDS);
        }

        for (Duration duration : timeoutsScreenOn) {
            executorService.schedule(createTask(), duration.getMillis(), TimeUnit.MILLISECONDS);
        }
    }

    public void reflect(Parameters parameters) {

        if (previousParameters != null) {
            for (Task task : tasks) {
                task.tryPerform(parameters);
            }

            if (parameters.getScreenStateChanged() == ScreenStateChanged.Disabled) {

                //screenStateSwitchedInstant.set(Instant.now());

                for (Duration duration : timeoutsScreenOff) {
                    executorService.schedule(createTask(), duration.getMillis(), TimeUnit.MILLISECONDS);
                }
            }

            if (parameters.getScreenStateChanged() == ScreenStateChanged.Enabled) {

                //screenStateSwitchedInstant.set(Instant.now());

                for (Duration duration : timeoutsScreenOn) {
                    executorService.schedule(createTask(), duration.getMillis(), TimeUnit.MILLISECONDS);
                }
            }
        }
    }

    private Parameters getParameters(){
        return getParameters(null);
    }

    private synchronized Parameters getParameters(CallBack callBack) {
        Parameters parameters = previousParameters != null ? new Parameters(previousParameters) : new Parameters();

        Parameters.fillCurrentTime(parameters);

        /*if(screenStateSwitchedInstant != null && parameters.getScreenState() == ScreenState.On){
            screenStateSwitchedInstant = null;
        }*/
        if(callBack != null) {
            callBack.invoke(parameters);
        }

        if (previousParameters.getScreenState() == ScreenState.On
                && parameters.getScreenState() == ScreenState.Off) {
            screenStateSwitchedInstant.set(Instant.now());
            parameters.setScreenStateChanged(ScreenStateChanged.Disabled);
        }

        if (previousParameters.getScreenState() == ScreenState.Off
                && parameters.getScreenState() == ScreenState.On) {
            screenStateSwitchedInstant.set(Instant.now());
            parameters.setScreenStateChanged(ScreenStateChanged.Enabled);
        }

        if (screenStateSwitchedInstant.get() != null) {
            parameters.setScreenStatePeriod(new Duration(screenStateSwitchedInstant.get(), Instant.now()));
        }

        previousParameters = parameters;

        return parameters;
    }

    private void taskAdded(Task task) {

        if(task.getAction() instanceof ActionVolume
                && !conditionNotAnyTask.getTasks().contains(task)){
            task.getConditions().add(conditionNotAnyTask);
        }

        for (Condition condition : task.getConditions()) {
            if (condition instanceof ConditionTime) {
                ConditionTime src = (ConditionTime) condition;
                Time time = src.getTime();
                long interval = getInterval(time.getHour(), time.getMinute(), false);

                Log.i("Battery", String.format("Interval: %s", interval));

                executorService.schedule(createTaskRepeatable(time), interval == 0 ? 10000 : interval, TimeUnit.MILLISECONDS);
            }

            if (condition instanceof ConditionTimeInterval) {
                ConditionTimeInterval src = (ConditionTimeInterval) condition;
                Time time = src.getFromTime();
                long interval = getInterval(time.getHour(), time.getMinute(), false);

                executorService.schedule(createTaskRepeatable(time), interval == 0 ? 10000 : interval, TimeUnit.MILLISECONDS);
            }

            if (condition instanceof ConditionTimeoutScreen) {
                ConditionTimeoutScreen src = (ConditionTimeoutScreen) condition;
                if (src.getScreenState() == ScreenState.On) {
                    if(!timeoutsScreenOn.contains(src.getDuration())) {
                        timeoutsScreenOn.add(src.getDuration());
                    }
                } else {
                    if(!timeoutsScreenOff.contains(src.getDuration())) {
                        timeoutsScreenOff.add(src.getDuration());
                    }
                }

            }
        }
    }

    private void taskRemoved(Task task) {

    }

    private void taskExternalAdded(Task task){
        //we need an live-timeout for task. When time's up task will die.
        /*if(task.getAction() instanceof ActionVolume) {
            conditionNotAnyTask.addTask(task);
            //task.getConditions().add(conditionNotAnyTask);
        }*/
    }

    private long getInterval(int desiredHour, int desiredMinute, boolean checkDay) {

        Calendar calendar = Calendar.getInstance();
        int currentMinute = calendar.get(Calendar.MINUTE);
        int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        long currentTimestamp = calendar.getTimeInMillis();

        if (checkDay && (desiredHour == currentHour && desiredMinute <= currentMinute || desiredHour < currentHour)) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        calendar.set(Calendar.HOUR_OF_DAY, desiredHour);
        calendar.set(Calendar.MINUTE, desiredMinute);
        calendar.set(Calendar.SECOND, 0);

        long diffTimestamp = calendar.getTimeInMillis() - currentTimestamp;

        return (diffTimestamp < 0 ? 0 : diffTimestamp);
    }

    private Runnable createTaskRepeatable(final Time time) {
        return new Runnable() {
            @Override
            public void run() {

                Parameters parameters = getParameters();

                reflect(parameters);

                long interval = getInterval(time.getHour(), time.getMinute(), true);
                if (interval != 0) {
                    executorService.schedule(createTaskRepeatable(time), interval, TimeUnit.MILLISECONDS);
                }
            }
        };
    }

    private Runnable createTask() {
        return new Runnable() {
            @Override
            public void run() {
                Parameters parameters = getParameters();
                reflect(parameters);
            }
        };
    }

    protected Tasks getTasks() {
        return tasks;
    }

    public void addTask(Task task){
        taskExternalAdded(task);
        tasks.add(task);
    }

    public void setPowerController(PowerController powerController) {
        PowerController old = this.powerController;
        this.powerController = powerController;

        onPowerControllerChanged(old, this.powerController);
    }

    public ScreenController getScreenController() {
        return screenController;
    }

    public void setScreenController(ScreenController screenController) {

        ScreenController old = this.screenController;
        this.screenController = screenController;
        onScreenControllerChanged(old, this.screenController);

    }

    private void onPowerControllerChanged(PowerController oldValue, PowerController newValue) {

        if (newValue != null) {
            newValue.getHandler().add(new ru.ut.profiler.commons.event.Listener<PowerEventArgs>() {
                @Override
                public void invoke(final PowerEventArgs args) {

                    CallBack callBack = new CallBack() {
                        @Override
                        public void invoke(Parameters parameters) {
                            parameters.setBatteryLevel(args.getLevel());
                            parameters.setCharging(args.getCharging());
                        }
                    };
                    reflect(getParameters(callBack));
                }
            });
        }
    }

    private void onScreenControllerChanged(ScreenController oldValue, ScreenController newValue) {
        if (newValue != null) {
            newValue.getHandler().add(new ru.ut.profiler.commons.event.Listener<ScreenEventArgs>() {
                @Override
                public void invoke(final ScreenEventArgs args) {
                    CallBack callBack = new CallBack() {
                        @Override
                        public void invoke(Parameters parameters) {
                            parameters.setScreenState(args.getScreenState());
                        }
                    };

                    reflect(getParameters(callBack));
                }
            });
        }
    }


    public ProfileController getProfileController() {
        return profileController;
    }

    public void setProfileController(ProfileController profileController) {
        this.profileController = profileController;
    }
}

package ru.ut.profiler.model.condition;

import org.joda.time.Duration;
import ru.ut.profiler.model.Parameters;
import ru.ut.profiler.model.enums.ScreenState;

/**
 * User: utegental
 * Date: 12.01.14
 * Time: 11:18
 */
public class ConditionTimeoutScreen extends ConditionTimeout {

    private ScreenState screenState;


    public ConditionTimeoutScreen(Duration duration, ScreenState screenState) {
        super(duration);
        this.screenState = screenState;
    }

    @Override
    public boolean check(Parameters parameters) {
        return this.periodIsOk(parameters.getScreenStatePeriod()) && this.screenState == parameters.getScreenState();
    }

    private boolean periodIsOk(Duration currentPeriod) {
        boolean result = false;

        if (currentPeriod != null) {
            int compare = currentPeriod.compareTo(this.getDuration());
            result = compare == 0 || compare == 1;
        }


        return result;
    }

    public ScreenState getScreenState() {
        return screenState;
    }
}

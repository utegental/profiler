package ru.ut.profiler.model.enums;

/**
 * User: utegental
 * Date: 06.01.14
 * Time: 1:47
 */
public enum Equation {
    Bigger,
    Less,
    Equals,
    BiggerOrEquals,
    LessOrEquals,
    NotEquals
}

package ru.ut.profiler.model.sp.builders;

/**
 * User: utegental
 * Date: 02.03.14
 * Time: 14:54
 */
public abstract class BuilderBase implements Builder {
    private BuilderParams params;

    public BuilderParams getParams() {
        return params;
    }

    @Override
    public void setParams(BuilderParams params) {
        this.params = params;
    }
}

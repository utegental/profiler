package ru.ut.profiler.model.condition;

import ru.ut.profiler.model.Parameters;
import ru.ut.profiler.model.Time;
import ru.ut.profiler.model.TimeInterval;
import ru.ut.profiler.model.enums.Compare;

/**
 * User: utegental
 * Date: 02.02.14
 * Time: 1:27
 */
public class ConditionTimeInterval implements Condition {

    private Time fromTime;
    private Time toTime;

    public ConditionTimeInterval(TimeInterval interval) {
        this.fromTime = interval.getFrom();
        this.toTime = interval.getTo();
    }

    public ConditionTimeInterval(Time fromTime, Time toTime) {
        this.fromTime = fromTime;
        this.toTime = toTime;
    }

    @Override
    public boolean check(Parameters parameters) {
        boolean result = false;
        boolean toTimeIsLower = toTime.compare(fromTime) == Compare.Lower;


        Time now = parameters.getTime();
        if (now != null) {
            Compare now2ToTime = now.compare(fromTime);
            boolean isGreatOrEqual = now2ToTime == Compare.Greater || now2ToTime == Compare.Equal;

            if (isGreatOrEqual && now.compare(toTime) == Compare.Lower
                    || toTimeIsLower && (isGreatOrEqual || now.compare(toTime) == Compare.Lower)) {
                result = true;
            }
        }

        return result;
    }

    public Time getFromTime() {
        return fromTime;
    }


}

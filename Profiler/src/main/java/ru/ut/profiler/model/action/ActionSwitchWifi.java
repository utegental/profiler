package ru.ut.profiler.model.action;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.util.Log;

/**
 * User: utegental
 * Date: 06.01.14
 * Time: 2:04
 */
public class ActionSwitchWifi implements Action {

    private boolean value;

    public ActionSwitchWifi(boolean value) {
        this.value = value;
    }

    @Override
    public synchronized void invoke(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager.isWifiEnabled() != value) {
            wifiManager.setWifiEnabled(value);
            Log.i("Battery", String.format("Wifi is switched to %s", value));
        }
    }
}

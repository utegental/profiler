package ru.ut.profiler.model.sp.ringer;

import ru.ut.profiler.model.Task;
import ru.ut.profiler.model.TimeInterval;
import ru.ut.profiler.model.action.ActionRingerMode;
import ru.ut.profiler.model.condition.ConditionTimeInterval;
import ru.ut.profiler.model.enums.RingerMode;

/**
 * User: utegental
 * Date: 02.03.14
 * Time: 14:31
 */
public class DayTask extends Task {
    public DayTask(TimeInterval dayInterval) {
        super(new ActionRingerMode(RingerMode.Normal));

        getConditions().add(new ConditionTimeInterval(dayInterval));
    }
}

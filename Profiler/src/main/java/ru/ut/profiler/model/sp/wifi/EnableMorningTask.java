package ru.ut.profiler.model.sp.wifi;

import ru.ut.profiler.model.TimeInterval;
import ru.ut.profiler.model.action.ActionSwitchWifi;
import ru.ut.profiler.model.condition.ConditionTimeInterval;
import ru.ut.profiler.model.sp.ScreenOnUnpluggedTask;

/**
 * User: utegental
 * Date: 01.03.14
 * Time: 22:54
 */
public class EnableMorningTask extends ScreenOnUnpluggedTask {
    public EnableMorningTask(TimeInterval morningInterval) {
        super(new ActionSwitchWifi(true));

        getConditions().add(new ConditionTimeInterval(morningInterval));
    }
}

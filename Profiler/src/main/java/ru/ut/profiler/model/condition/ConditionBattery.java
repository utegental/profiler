package ru.ut.profiler.model.condition;

import android.util.Log;
import ru.ut.profiler.model.Parameters;
import ru.ut.profiler.model.enums.Charging;
import ru.ut.profiler.model.enums.Equation;

/**
 * User: utegental
 * Date: 04.01.14
 * Time: 12:16
 */
public class ConditionBattery implements Condition {
    private Charging charging;
    private int level;
    private Equation equation;

    public ConditionBattery(Charging charging) {
        this.charging = charging;

        this.level = 0;
        this.equation = Equation.BiggerOrEquals;
    }

    public ConditionBattery(int level, Equation equation, Charging charging) {
        this.level = level;
        this.equation = equation;
        this.charging = charging;
    }

    public Charging getCharging() {
        return charging;
    }

    public int getLevel() {
        return level;
    }

    @Override
    public boolean check(Parameters parameters) {
        boolean result = true;

        Charging charging = parameters.getCharging();
        if (charging != Charging.Unknown && this.charging != Charging.DoesntMatter && charging != this.charging) {
            result = false;
        }

        result = result && batteryLevelIsValid(parameters.getBatteryLevel());

        return result;
    }

    private boolean batteryLevelIsValid(int batteryLevel) {
        boolean result = false;
        switch (equation) {
            case Bigger:
                result = batteryLevel > this.level;
                break;
            case BiggerOrEquals:
                result = batteryLevel >= this.level;
                break;
            case Equals:
                result = batteryLevel == this.level;
                break;
            case NotEquals:
                result = batteryLevel != this.level;
                break;
            case Less:
                result = batteryLevel < this.level;
                break;
            case LessOrEquals:
                result = batteryLevel <= this.level;
                if (result) {
                    Log.i("Battery", String.format("BatteryLevel - %s, ConditionLevel - %s", batteryLevel, this.level));
                }
                break;
        }

        return result;
    }
}

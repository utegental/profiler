package ru.ut.profiler.model.condition;

import org.joda.time.Duration;

/**
 * User: utegental
 * Date: 12.01.14
 * Time: 9:38
 */
public abstract class ConditionTimeout implements Condition {

    private Duration duration;
    /*private Time time;*/

    /*public ConditionTimeout(Time time) {
        this.time = time;
    }*/
    public ConditionTimeout(Duration duration) {
        this.duration = duration;

    }

    /*public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }*/

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }
}

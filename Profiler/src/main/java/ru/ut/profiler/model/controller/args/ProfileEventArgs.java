package ru.ut.profiler.model.controller.args;

import ru.ut.profiler.commons.event.EventArgs;
import ru.ut.profiler.model.Profile;

/**
 * User: utegental
 * Date: 03.02.14
 * Time: 23:11
 */
public class ProfileEventArgs extends EventArgs {

    private final Profile profile;

    public ProfileEventArgs(Profile profile) {
        this.profile = profile;
    }

    public Profile getProfile() {
        return profile;
    }
}

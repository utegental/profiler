package ru.ut.profiler.model.condition;

import ru.ut.profiler.model.Parameters;

/**
 * User: utegental
 * Date: 04.01.14
 * Time: 12:32
 */
public class ConditionAnd implements Condition {
    private Condition firstCondition;
    private Condition secondCondition;

    public ConditionAnd(Condition firstCondition, Condition secondCondition) {
        this.firstCondition = firstCondition;
        this.secondCondition = secondCondition;
    }

    public Condition getFirstCondition() {
        return firstCondition;
    }

    public Condition getSecondCondition() {
        return secondCondition;
    }

    @Override
    public boolean check(Parameters parameters) {
        return firstCondition.check(parameters) && secondCondition.check(parameters);
    }
}

package ru.ut.profiler.model;

import ru.ut.profiler.model.enums.Compare;

/**
 * User: utegental
 * Date: 01.02.14
 * Time: 23:05
 */
public class Time {
    private int hour;
    private int minute;

    public Time(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    /**
     * @param other
     * @return This is greater/lower/equal than {@code other}
     */
    public Compare compare(Time other) {
        Compare result;

        if (hour == other.getHour()) {
            result = compareMinute(other.getMinute());
        } else if (hour < other.getHour()) {
            result = Compare.Lower;
        } else {
            result = Compare.Greater;
        }

        return result;
    }

    private Compare compareMinute(int otherMinute) {
        Compare result;

        if (minute == otherMinute) {
            result = Compare.Equal;
        } else if (minute < otherMinute) {
            result = Compare.Lower;
        } else {
            result = Compare.Greater;
        }

        return result;
    }
}

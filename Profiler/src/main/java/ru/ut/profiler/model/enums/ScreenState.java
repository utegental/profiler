package ru.ut.profiler.model.enums;

/**
 * User: utegental
 * Date: 07.01.14
 * Time: 14:12
 */
public enum ScreenState {
    Unknown((short) 0),
    On((short) 1),
    Off((short) 2);

    public short value;

    ScreenState(short value) {
        this.value = value;
    }

    public static ScreenState parse(short value) {
        ScreenState result = ScreenState.Unknown;
        for (ScreenState charging : ScreenState.values()) {
            if (charging.value == value) {
                result = charging;
                break;
            }
        }
        return result;
    }
}

package ru.ut.profiler.model;

/**
 * Created by Pavel on 06.04.2014.
 */
public interface CallBack {
    void invoke(Parameters parameters);
}

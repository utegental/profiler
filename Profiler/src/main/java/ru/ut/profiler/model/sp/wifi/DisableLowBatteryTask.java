package ru.ut.profiler.model.sp.wifi;

import ru.ut.profiler.model.Task;
import ru.ut.profiler.model.action.ActionSwitchWifi;
import ru.ut.profiler.model.sp.Conditions;

/**
 * User: utegental
 * Date: 02.03.14
 * Time: 21:51
 */
public class DisableLowBatteryTask extends Task {
    public DisableLowBatteryTask() {
        super(new ActionSwitchWifi(false));

        getConditions().add(Conditions.lowBatteryCondition());
    }
}

package ru.ut.profiler.model.sp;

import org.joda.time.Duration;
import ru.ut.profiler.model.condition.Condition;
import ru.ut.profiler.model.condition.ConditionBattery;
import ru.ut.profiler.model.condition.ConditionTimeoutScreen;
import ru.ut.profiler.model.enums.Charging;
import ru.ut.profiler.model.enums.Equation;
import ru.ut.profiler.model.enums.ScreenState;

/**
 * User: utegental
 * Date: 09.03.14
 * Time: 21:26
 */
public class Conditions {

    //private static final int BATTERY_NORMAL_LEVEL = 50;
    private static final int BATTERY_LOW_LEVEL = 30;
    private static ConditionBattery lowBatteryCondition = new ConditionBattery(BATTERY_LOW_LEVEL, Equation.LessOrEquals, Charging.Unplugged);
    private static ConditionBattery wellChargedCondition = new ConditionBattery(BATTERY_LOW_LEVEL, Equation.Bigger, Charging.DoesntMatter);
    private static final int BATTERY_VERY_LOW_LEVEL = 15;
    private static ConditionBattery acChargingCondition = new ConditionBattery(BATTERY_VERY_LOW_LEVEL, Equation.Bigger, Charging.AC);
    /**
     * Screen off timeout in minutes.
     */
    private static final int SCREEN_OFF_TIMEOUT = 10;
    private static ConditionTimeoutScreen screenOffTimeoutCondition = new ConditionTimeoutScreen(Duration.standardSeconds(SCREEN_OFF_TIMEOUT), ScreenState.Off);
    private static final int SCREEN_ON_TIMEOUT = 5;
    private static ConditionTimeoutScreen screenOnTimeoutCondition = new ConditionTimeoutScreen(Duration.standardMinutes(SCREEN_ON_TIMEOUT), ScreenState.On);



    public static Condition lowBatteryCondition() {
        return lowBatteryCondition;
    }

    public static Condition wellChargedCondition() {
        return wellChargedCondition;
    }

    public static Condition acChargingCondition() {
        return acChargingCondition;
    }

    public static Condition screenOffTimeoutCondition() {
        return screenOffTimeoutCondition;
    }

    public static Condition screenOnTimeoutCondition(){ return screenOnTimeoutCondition;}
}

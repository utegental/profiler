package ru.ut.profiler.model.sp.builders;

import ru.ut.profiler.model.Task;
import ru.ut.profiler.model.sp.volume.DayTask;
import ru.ut.profiler.model.sp.volume.NightTask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Pavel on 06.04.2014.
 */
public class VolumeBuilder extends BuilderBase {
    @Override
    public Collection<Task> createTasks() {
        BuilderParams params = getParams();
        return new ArrayList<>(Arrays.asList(new DayTask(params.getDay()), new NightTask(params.getNight())));
    }
}

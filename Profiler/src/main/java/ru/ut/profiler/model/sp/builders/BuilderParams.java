package ru.ut.profiler.model.sp.builders;

import ru.ut.profiler.model.TimeInterval;

/**
 * User: utegental
 * Date: 02.03.14
 * Time: 15:02
 */
public class BuilderParams {
    private TimeInterval day;
    private TimeInterval night;
    private TimeInterval morning;
    private TimeInterval evening;

    public TimeInterval getDay() {
        return day;
    }

    public void setDay(TimeInterval day) {
        this.day = day;
    }

    public TimeInterval getNight() {
        return night;
    }

    public void setNight(TimeInterval night) {
        this.night = night;
    }

    public TimeInterval getMorning() {
        return morning;
    }

    public void setMorning(TimeInterval morning) {
        this.morning = morning;
    }

    public TimeInterval getEvening() {
        return evening;
    }

    public void setEvening(TimeInterval evening) {
        this.evening = evening;
    }
}

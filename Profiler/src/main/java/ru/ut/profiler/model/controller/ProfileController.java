package ru.ut.profiler.model.controller;

import ru.ut.profiler.commons.event.Handler;
import ru.ut.profiler.model.Profile;
import ru.ut.profiler.model.controller.args.ProfileEventArgs;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * User: utegental
 * Date: 03.02.14
 * Time: 10:25
 */
public class ProfileController {

    private final ProfileHandler handler = new ProfileHandler();
    private final Map<Integer, Profile> profiles = new HashMap<>();
    private final Random random = new Random();

    private Integer activeId;

    public void setActive(Integer id) {

        if (activeId != id) {
            activeId = id;
            handler.setActive(profiles.get(id));
        }
    }

    public Integer addProfile(Profile profile) {
        Integer result = random.nextInt();
        profiles.put(result, profile);

        return result;
    }

    public ProfileHandler getHandler() {
        return handler;
    }


    public static class ProfileHandler extends Handler<ProfileEventArgs> {
        private void setActive(Profile profile) {
            this.invoke(new ProfileEventArgs(profile));
        }
    }
}

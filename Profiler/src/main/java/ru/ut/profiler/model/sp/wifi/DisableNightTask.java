package ru.ut.profiler.model.sp.wifi;

import ru.ut.profiler.model.Task;
import ru.ut.profiler.model.TimeInterval;
import ru.ut.profiler.model.action.ActionSwitchWifi;
import ru.ut.profiler.model.condition.Condition;
import ru.ut.profiler.model.condition.ConditionBattery;
import ru.ut.profiler.model.condition.ConditionTimeInterval;
import ru.ut.profiler.model.enums.Charging;
import ru.ut.profiler.model.sp.Conditions;

import java.util.List;

/**
 * User: utegental
 * Date: 01.03.14
 * Time: 22:48
 */
public class DisableNightTask extends Task {
    public DisableNightTask(TimeInterval nightInterval) {
        super(new ActionSwitchWifi(false));

        final List<Condition> conditions = this.getConditions();

        conditions.add(new ConditionTimeInterval(nightInterval));
        conditions.add(new ConditionBattery(Charging.Unplugged));
        conditions.add(Conditions.screenOffTimeoutCondition());
    }
}

package ru.ut.profiler.model.sp;

import ru.ut.profiler.model.Task;
import ru.ut.profiler.model.action.Action;

/**
 * User: utegental
 * Date: 01.03.14
 * Time: 22:37
 */
public abstract class UnpluggedBase extends Task {
    public UnpluggedBase(Action action) {
        super(action);

        this.getConditions().add(Conditions.wellChargedCondition());
    }
}

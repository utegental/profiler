package ru.ut.profiler.model.action;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * User: utegental
 * Date: 06.01.14
 * Time: 1:33
 */
public class ActionSwitchMobileData implements Action {

    private boolean value;

    public ActionSwitchMobileData(boolean value) {
        this.value = value;
    }

    @Override
    public synchronized void invoke(Context context) {
        try {
            this.setMobileDataEnabled(context, value);
            Log.i("Battery", String.format("Mobile data is switched to %s", value));
        } catch (Exception exception) {
            Log.e("Battery", "", exception);
        }

    }


    private void setMobileDataEnabled(Context context, boolean value) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        final ConnectivityManager conman = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        final Class conmanClass = Class.forName(conman.getClass().getName());
        final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
        iConnectivityManagerField.setAccessible(true);
        final Object iConnectivityManager = iConnectivityManagerField.get(conman);
        final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
        final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
        setMobileDataEnabledMethod.setAccessible(true);

        setMobileDataEnabledMethod.invoke(iConnectivityManager, value);
    }
}

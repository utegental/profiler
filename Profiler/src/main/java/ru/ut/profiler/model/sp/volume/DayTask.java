package ru.ut.profiler.model.sp.volume;

import ru.ut.profiler.model.Task;
import ru.ut.profiler.model.TimeInterval;
import ru.ut.profiler.model.action.ActionVolume;
import ru.ut.profiler.model.condition.ConditionTimeInterval;

/**
 * Created by Pavel on 06.04.2014.
 */
public class DayTask extends Task {
    public DayTask(TimeInterval dayInterval) {
        super(new ActionVolume(7, 7));
        getConditions().add(new ConditionTimeInterval(dayInterval));
    }
}

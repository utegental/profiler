package ru.ut.profiler.model.controller;

import ru.ut.profiler.commons.event.EventArgs;
import ru.ut.profiler.commons.event.Handler;
import ru.ut.profiler.commons.event.Listener;
import ru.ut.profiler.receiver.ReceiverBase;

/**
 * User: utegental
 * Date: 03.02.14
 * Time: 17:18
 */
public abstract class ControllerBase<T extends EventArgs, S extends EventArgs> {
    private final InnerHandler<T> handler = initHandler();
    private ReceiverBase<? extends Handler<S>> receiver;

    private final Listener<S> listener = new Listener<S>() {
        @Override
        public void invoke(S args) {
            ControllerBase.this.handler.onChanged(initArgs(args));
        }
    };

    protected abstract T initArgs(S args);

    protected InnerHandler<T> initHandler() {
        return new InnerHandler<>();
    }

    public Handler<T> getHandler() {
        return handler;
    }

    public ReceiverBase<? extends Handler<S>> getReceiver() {
        return receiver;
    }

    public void setReceiver(ReceiverBase<? extends Handler<S>> receiver) {
        ReceiverBase<? extends Handler<S>> old = this.receiver;
        this.receiver = receiver;
        this.onReceiverChanged(old, this.receiver);
    }

    protected void onReceiverChanged(ReceiverBase<? extends Handler<S>> oldValue, ReceiverBase<? extends Handler<S>> newValue) {
        if (oldValue != null) {
            oldValue.getHandler().remove(listener);
        }

        if (newValue != null) {
            newValue.getHandler().add(listener);
        }
    }

    public static class InnerHandler<T extends EventArgs> extends ru.ut.profiler.commons.event.Handler<T> {
        private void onChanged(T args) {
            this.invoke(args);
        }
    }
}

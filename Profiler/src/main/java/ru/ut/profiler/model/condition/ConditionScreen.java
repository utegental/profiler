package ru.ut.profiler.model.condition;

import ru.ut.profiler.model.Parameters;
import ru.ut.profiler.model.enums.ScreenState;

/**
 * User: utegental
 * Date: 07.01.14
 * Time: 23:10
 */
public class ConditionScreen implements Condition {

    private ScreenState screenState;

    public ConditionScreen(ScreenState screenState) {
        this.screenState = screenState;
    }


    @Override
    public boolean check(Parameters parameters) {
        return parameters.getScreenState() == this.screenState;
    }

    public ScreenState getScreenState() {
        return screenState;
    }
}

package ru.ut.profiler.model.controller;

import ru.ut.profiler.model.controller.args.PowerEventArgs;

/**
 * User: utegental
 * Date: 03.02.14
 * Time: 11:11
 */
public class PowerController extends ControllerBase<PowerEventArgs, ru.ut.profiler.receiver.args.PowerEventArgs> {

    @Override
    protected PowerEventArgs initArgs(ru.ut.profiler.receiver.args.PowerEventArgs args) {
        return new PowerEventArgs(args.getLevel(), args.getCharging());
    }
}

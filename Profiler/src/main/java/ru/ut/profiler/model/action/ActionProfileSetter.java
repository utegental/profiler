package ru.ut.profiler.model.action;

import android.content.Context;
import ru.ut.profiler.model.controller.ProfileController;

/**
 * User: utegental
 * Date: 03.02.14
 * Time: 23:20
 */
public class ActionProfileSetter implements Action {

    private final ProfileController profileController;
    private final Integer profileId;

    public ActionProfileSetter(ProfileController profileController, Integer profileId) {
        this.profileController = profileController;
        this.profileId = profileId;
    }

    @Override
    public void invoke(Context context) {
        profileController.setActive(profileId);

    }
}

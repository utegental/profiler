package ru.ut.profiler.model.controller;

import ru.ut.profiler.model.Time;
import ru.ut.profiler.model.controller.args.VolumeEventArgs;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * User: utegental
 * Date: 02.02.14
 * Time: 18:47
 */
public class VolumeController extends ControllerBase<VolumeEventArgs, ru.ut.profiler.receiver.args.VolumeEventArgs> {
    private Timer timer = new Timer();
    private AtomicBoolean isShowed = new AtomicBoolean();
    private AtomicBoolean isHided = new AtomicBoolean();

    public boolean canShow() {
        return !isHided.get();
    }

    public boolean getIsShowed() {
        return isShowed.get();
    }

    public void setIsShowed(boolean isShowed) {
        this.isShowed.set(isShowed);
    }


    public void hide() {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                isHided.set(false);
            }
        };
        isHided.set(true);
        timer.schedule(task, 10000);
    }

    public void hide(Time time) {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                isHided.set(false);
            }
        };
        isHided.set(true);

        long interval = 1000 * 60 * (time.getMinute() + 60 * time.getHour());
        timer.schedule(task, interval);
    }

    @Override
    protected VolumeEventArgs initArgs(ru.ut.profiler.receiver.args.VolumeEventArgs args) {
        return new VolumeEventArgs(args.getRingerValue(), args.getMediaVolumeValue());
    }
}

package ru.ut.profiler.model.sp.volume;

import ru.ut.profiler.model.Task;
import ru.ut.profiler.model.TimeInterval;
import ru.ut.profiler.model.action.ActionVolume;
import ru.ut.profiler.model.condition.ConditionTimeInterval;

/**
 * Created by Pavel on 06.04.2014.
 */
public class NightTask extends Task {
    public NightTask(TimeInterval nightInterval) {
        super(new ActionVolume(2, 2));
        getConditions().add(new ConditionTimeInterval(nightInterval));
    }
}

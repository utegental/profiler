package ru.ut.profiler.model.action;

import android.content.Context;
import android.media.AudioManager;
import ru.ut.profiler.model.enums.RingerMode;

/**
 * User: utegental
 * Date: 01.02.14
 * Time: 22:48
 */
public class ActionRingerMode implements Action {

    private RingerMode value;

    public ActionRingerMode(RingerMode value) {
        this.value = value;
    }

    @Override
    public void invoke(Context context) {
        AudioManager manager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        int newValue;
        switch (value) {
            case Silent:
                newValue = AudioManager.RINGER_MODE_SILENT;
                break;
            case Vibrating:
                newValue = AudioManager.RINGER_MODE_VIBRATE;
                break;
            case Normal:
            default:
                newValue = AudioManager.RINGER_MODE_NORMAL;
                break;
        }

        manager.setRingerMode(newValue);
    }
}

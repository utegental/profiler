package ru.ut.profiler.model.enums;

/**
 * Created by Pavel on 06.04.2014.
 */
public enum ScreenStateChanged {
    Unknown((short) 0),
    NotChanged((short) 1),
    Enabled((short) 2),
    Disabled((short) 3);

    public short value;

    ScreenStateChanged(short value) {
        this.value = value;
    }

    public static ScreenStateChanged parse(short value) {
        ScreenStateChanged result = ScreenStateChanged.Unknown;
        for (ScreenStateChanged charging : ScreenStateChanged.values()) {
            if (charging.value == value) {
                result = charging;
                break;
            }
        }
        return result;
    }
}

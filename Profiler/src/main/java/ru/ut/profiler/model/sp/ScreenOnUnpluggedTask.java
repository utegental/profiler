package ru.ut.profiler.model.sp;

import ru.ut.profiler.model.action.Action;
import ru.ut.profiler.model.condition.ConditionScreen;
import ru.ut.profiler.model.enums.ScreenState;

/**
 * User: utegental
 * Date: 02.03.14
 * Time: 15:20
 */
public class ScreenOnUnpluggedTask extends UnpluggedBase {
    public ScreenOnUnpluggedTask(Action action) {
        super(action);
        getConditions().add(new ConditionScreen(ScreenState.On));
    }
}

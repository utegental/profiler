package ru.ut.profiler.model.condition;

import ru.ut.profiler.model.Parameters;

/**
 * User: utegental
 * Date: 04.01.14
 * Time: 11:13
 */
public interface Condition {
    boolean check(Parameters parameters);
}

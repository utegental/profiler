package ru.ut.profiler.model;

import ru.ut.profiler.model.action.Action;
import ru.ut.profiler.model.condition.Condition;

import java.util.LinkedList;
import java.util.List;

/**
 * User: utegental
 * Date: 04.01.14
 * Time: 12:39
 */
public class Task implements ITask {
    private final List<Condition> conditions = new LinkedList<Condition>();
    private Action action;

    public Task(Action action) {
        this.action = action;
    }

    public List<Condition> getConditions() {
        return conditions;
    }

    public Action getAction() {
        return action;
    }

    @Override
    public boolean check(Parameters parameters) {
        boolean match = false;
        for (Condition condition : conditions) {
            match = condition.check(parameters);
            if (!match) {
                break;
            }
        }
        return match;
    }

    @Override
    public void tryPerform(Parameters parameters) {
        if (check(parameters)) {
            action.invoke(parameters.getContext());
        }
    }
}

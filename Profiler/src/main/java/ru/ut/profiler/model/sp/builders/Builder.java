package ru.ut.profiler.model.sp.builders;

import ru.ut.profiler.model.Task;

import java.util.Collection;

/**
 * User: utegental
 * Date: 02.03.14
 * Time: 14:55
 */
public interface Builder {
    void setParams(BuilderParams params);

    Collection<Task> createTasks();
}

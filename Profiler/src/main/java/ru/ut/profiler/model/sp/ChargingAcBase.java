package ru.ut.profiler.model.sp;

import ru.ut.profiler.model.Task;
import ru.ut.profiler.model.action.Action;

/**
 * User: utegental
 * Date: 01.03.14
 * Time: 22:36
 */
public abstract class ChargingAcBase extends Task {
    public ChargingAcBase(Action action) {
        super(action);

        getConditions().add(Conditions.acChargingCondition());
    }
}

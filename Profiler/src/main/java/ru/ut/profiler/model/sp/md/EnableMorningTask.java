package ru.ut.profiler.model.sp.md;

import ru.ut.profiler.model.TimeInterval;
import ru.ut.profiler.model.action.ActionSwitchMobileData;
import ru.ut.profiler.model.condition.ConditionTimeInterval;
import ru.ut.profiler.model.sp.ScreenOnUnpluggedDelayedTask;

/**
 * Created by Pavel on 05.04.2014.
 */
public class EnableMorningTask extends ScreenOnUnpluggedDelayedTask {
    public EnableMorningTask(TimeInterval eveningInterval) {
        super(new ActionSwitchMobileData(true));
        getConditions().add(new ConditionTimeInterval(eveningInterval));
    }
}

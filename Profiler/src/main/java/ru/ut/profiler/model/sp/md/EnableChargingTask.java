package ru.ut.profiler.model.sp.md;

import ru.ut.profiler.model.action.ActionSwitchMobileData;
import ru.ut.profiler.model.sp.ChargingAcBase;

/**
 * User: utegental
 * Date: 03.03.14
 * Time: 9:18
 */
public class EnableChargingTask extends ChargingAcBase {
    public EnableChargingTask() {
        super(new ActionSwitchMobileData(true));
    }
}

package ru.ut.profiler.model.condition;

import ru.ut.profiler.model.Parameters;
import ru.ut.profiler.model.Time;

/**
 * User: utegental
 * Date: 04.01.14
 * Time: 12:15
 */
public class ConditionTime implements Condition {

    private Time time;

    public ConditionTime(int desiredHour, int desiredMinute) {
        this.time = new Time(desiredHour, desiredMinute);
    }

    @Override
    public boolean check(Parameters parameters) {
        boolean result = false;

        Time now = parameters.getTime();
        if (now != null && now.getHour() == time.getHour() && now.getMinute() == time.getMinute()) {
            result = true;
        }

        return result;
    }

    public Time getTime() {
        return time;
    }
}

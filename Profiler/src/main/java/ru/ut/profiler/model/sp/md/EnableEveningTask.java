package ru.ut.profiler.model.sp.md;

import ru.ut.profiler.model.TimeInterval;
import ru.ut.profiler.model.action.ActionSwitchMobileData;
import ru.ut.profiler.model.condition.ConditionTimeInterval;
import ru.ut.profiler.model.sp.ScreenOnUnpluggedDelayedTask;

/**
 * Created by Pavel on 05.04.2014.
 */
public class EnableEveningTask extends ScreenOnUnpluggedDelayedTask {
    public EnableEveningTask(TimeInterval eveningInterval) {
        super(new ActionSwitchMobileData(true));
        getConditions().add(new ConditionTimeInterval(eveningInterval));
    }
}

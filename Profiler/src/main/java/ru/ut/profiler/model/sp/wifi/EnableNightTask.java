package ru.ut.profiler.model.sp.wifi;

import ru.ut.profiler.model.TimeInterval;
import ru.ut.profiler.model.action.ActionSwitchWifi;
import ru.ut.profiler.model.condition.ConditionTimeInterval;
import ru.ut.profiler.model.sp.ScreenOnUnpluggedTask;

/**
 * User: utegental
 * Date: 01.03.14
 * Time: 23:10
 */
public class EnableNightTask extends ScreenOnUnpluggedTask {
    public EnableNightTask(TimeInterval nightInterval) {
        super(new ActionSwitchWifi(true));

        getConditions().add(new ConditionTimeInterval(nightInterval));
    }
}

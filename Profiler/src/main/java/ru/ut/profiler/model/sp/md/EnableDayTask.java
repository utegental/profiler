package ru.ut.profiler.model.sp.md;

import ru.ut.profiler.model.TimeInterval;
import ru.ut.profiler.model.action.ActionSwitchMobileData;
import ru.ut.profiler.model.condition.ConditionTimeInterval;
import ru.ut.profiler.model.sp.ScreenOnUnpluggedDelayedTask;

/**
 * User: utegental
 * Date: 01.03.14
 * Time: 23:24
 */
public class EnableDayTask extends ScreenOnUnpluggedDelayedTask {
    public EnableDayTask(TimeInterval dayInterval) {
        super(new ActionSwitchMobileData(true));

        getConditions().add(new ConditionTimeInterval(dayInterval));
    }
}

package ru.ut.profiler.model.enums;

/**
 * User: utegental
 * Date: 08.01.14
 * Time: 21:12
 */
public enum ServiceState {
    Unknown((short) 0),
    Started((short) 1),
    Stopped((short) 2);

    public short value;

    ServiceState(short value) {
        this.value = value;
    }

    public static ServiceState parse(short value) {
        ServiceState result = ServiceState.Unknown;
        for (ServiceState charging : ServiceState.values()) {
            if (charging.value == value) {
                result = charging;
                break;
            }
        }
        return result;
    }
}

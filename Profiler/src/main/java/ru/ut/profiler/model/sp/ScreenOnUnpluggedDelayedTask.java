package ru.ut.profiler.model.sp;

import ru.ut.profiler.model.action.Action;

/**
 * Created by Pavel on 06.04.2014.
 */
public class ScreenOnUnpluggedDelayedTask extends UnpluggedBase {
    public ScreenOnUnpluggedDelayedTask(Action action) {
        super(action);
        getConditions().add(Conditions.screenOnTimeoutCondition());
    }
}

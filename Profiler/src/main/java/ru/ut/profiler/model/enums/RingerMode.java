package ru.ut.profiler.model.enums;

/**
 * User: utegental
 * Date: 04.02.14
 * Time: 22:11
 */
public enum RingerMode {
    Normal,
    Vibrating,
    Silent
}

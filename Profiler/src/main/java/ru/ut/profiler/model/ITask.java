package ru.ut.profiler.model;

/**
 * Created by Pavel on 06.04.2014.
 */
public interface ITask {
    boolean check(Parameters parameters);
    void tryPerform(Parameters parameters);
}

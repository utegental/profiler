package ru.ut.profiler.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.*;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;
import ru.ut.profiler.MainActivity;
import ru.ut.profiler.R;
import ru.ut.profiler.commons.event.Listener;
import ru.ut.profiler.model.DecisionMakingCenter;
import ru.ut.profiler.model.Task;
import ru.ut.profiler.model.action.ActionVolume;
import ru.ut.profiler.model.controller.PowerController;
import ru.ut.profiler.model.controller.ProfileController;
import ru.ut.profiler.model.controller.ScreenController;
import ru.ut.profiler.model.controller.VolumeController;
import ru.ut.profiler.model.controller.args.ProfileEventArgs;
import ru.ut.profiler.model.controller.args.VolumeEventArgs;
import ru.ut.profiler.model.enums.ServiceState;
import ru.ut.profiler.receiver.*;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

/**
 * User: utegental
 * Date: 03.01.14
 * Time: 23:13
 */
public class MainService extends Service {
    private final VolumeController volumeController = new VolumeController();
    private final ProfileController profileController = new ProfileController();
    private final PowerController powerController = new PowerController();
    private final ScreenController screenController = new ScreenController();
    private final DecisionMakingCenter decisionMakingCenter = new DecisionMakingCenter();
    private final ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(5);
    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new LocalBinder();
    private NotificationManager mNM;
    // Unique Identification Number for the Notification.
    // We use it on Notification start, and to cancel it.
    private int NOTIFICATION = R.string.local_service_started;
    private String[] powerIntents = new String[]{Intent.ACTION_BATTERY_CHANGED,
            Intent.ACTION_BATTERY_LOW,
            Intent.ACTION_BATTERY_OKAY,
            Intent.ACTION_POWER_CONNECTED,
            Intent.ACTION_POWER_DISCONNECTED
    };

    @Override
    public void onCreate() {
        mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);


        // Display a notification about us starting.  We put an icon in the status bar.
        showNotification();

        /*try {
            final TimePickerDialog.Builder builder = new TimePickerDialog.Builder(this);
            //final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Test dialog");
        *//*builder.setIcon(R.drawable.icon);*//*
            builder.setMessage("Content");


            builder.setPositiveButton("Да", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();


                }
            });

            builder.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            *//*AlertDialog alert = builder.create();
            alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            alert.show();*//*

            TimePickerDialog dialog2 = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int i, int i2) {

                }
            }, 2, 0, true);
            dialog2.setTitle("Возобновить контроль звука?");
            dialog2.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            dialog2.setButton(DialogInterface.BUTTON_NEGATIVE, "Нет", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            dialog2.setButton(DialogInterface.BUTTON_POSITIVE, "Да", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            dialog2.show();
        } catch (Throwable exception) {
            Log.e("Battery)", "Error", exception);
        }*/
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
        final ContextWrapper wrapper = new ContextWrapper(getBaseContext());

        PowerReceiver powerReceiver = new PowerReceiver();
        ScreenReceiver screenReceiver = new ScreenReceiver();
        VolumeReceiver volumeReceiver = new VolumeReceiver();
        MessageReceiver messageReceiver = new MessageReceiver();


        messageReceiver.setVolumeController(volumeController);

        powerController.setReceiver(powerReceiver);
        screenController.setReceiver(screenReceiver);
        volumeController.setReceiver(volumeReceiver);

        decisionMakingCenter.setPowerController(powerController);
        decisionMakingCenter.setScreenController(screenController);
        decisionMakingCenter.setProfileController(profileController);

        decisionMakingCenter.setContext(wrapper);
        decisionMakingCenter.initCenter();

        //wrapper.registerReceiver(messageReceiver, new IntentFilter(CustomIntent.STATE));
        /*wrapper.registerReceiver(screenReceiver, new IntentFilter(Intent.ACTION_SCREEN_ON));
        wrapper.registerReceiver(screenReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));
*/
        //wrapper.registerReceiver(volumeReceiver, new IntentFilter("android.media.VOLUME_CHANGED_ACTION"));


        volumeController.getHandler().add(new Listener<VolumeEventArgs>() {
            @Override
            public void invoke(VolumeEventArgs args) {
                onVolumeChanged(wrapper, args);
            }
        });

        volumeReceiver.start(wrapper);
        screenReceiver.start(wrapper);
        powerReceiver.start(wrapper);

        //registerMediaButtonEventReceiver

        /*((AudioManager)getSystemService(AUDIO_SERVICE)).registerMediaButtonEventReceiver(new ComponentName(
                this,
                VolumeReceiver.class));*/

        /*wrapper.registerReceiver(volumeReceiver, new IntentFilter("android.media.EXTRA_VOLUME_STREAM_VALUE"));*/



        /*for (String actionName : powerIntents) {
            wrapper.registerReceiver(powerReceiver, new IntentFilter(actionName));
        }*/


        final PendingIntent contentIntent = PendingIntent.getActivity(this, 1,
                new Intent(this, MainActivity.class), 1);


        Notification notification = builder
                .setContentTitle("Profiler")
                .setContentText("Пробный режим")
                .setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.ic_launcher)
                .build();


        startForeground(1, notification);

        profileController.getHandler().add(new Listener<ProfileEventArgs>() {
            @Override
            public void invoke(ProfileEventArgs args) {

                PendingIntent contentIntent = PendingIntent.getActivity(wrapper, 1,
                        new Intent(wrapper, MainActivity.class), 1);

                Notification notification = builder.setContentTitle("Profiler")
                        .setContentText(args.getProfile().getName())
                        .setContentIntent(contentIntent)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .build();
                mNM.notify(1, notification);
            }
        });

        /*BroadcastReceiver receiver1 = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);


                if (level != -1 && scale != -1) {
                    float batteryPct = level * 100 / (float) scale;

                    PendingIntent contentIntent = PendingIntent.getActivity(context, 1,
                            new Intent(context, MainActivity.class), 1);


                    *//*Notification notification = builder.setContentTitle("Profiler")
                            .setContentText(String.format("Пробный режим. %s%%", batteryPct))
                            .setContentIntent(contentIntent)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .build();
                    mNM.notify(1, notification);*//*
                }
            }
        };*/


        //registerPowerReceiver(wrapper, receiver1);

        Log.i("Battery", "Received start id " + startId + ": " + intent);

        Intent i = new Intent(CustomIntent.SERVICE);
        i.putExtra(ServiceManager.SERVICE_STATE, ServiceState.Started.value);
        wrapper.sendBroadcast(i);

        Toast.makeText(this, R.string.local_service_started, Toast.LENGTH_SHORT).show();
        return START_STICKY;
    }

    private void onVolumeChanged(Context context, VolumeEventArgs args) {

        Task task = new Task(new ActionVolume(args.getRingerValue(), args.getMediaVolumeValue()));

        decisionMakingCenter.addTask(task);
        /*if (volumeController.getIsShowed() || !volumeController.canShow()) return;

        try {
            final Time time = new Time(2, 0);
            TimePickerDialog dialog2 = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int i, int i2) {

                    time.setHour(i);
                    time.setMinute(i2);
                }
            }, time.getHour(), time.getMinute(), true);
            dialog2.setTitle("Возобновить контроль звука через указанный интервал времени?");
            dialog2.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);

            dialog2.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    volumeController.setIsShowed(false);
                }
            });

            dialog2.setButton(DialogInterface.BUTTON_NEGATIVE, "Нет", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    volumeController.hide();
                }
            });
            dialog2.setButton(DialogInterface.BUTTON_POSITIVE, "Да", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    volumeController.hide(time);
                }
            });
            dialog2.show();
            volumeController.setIsShowed(true);
        } catch (Throwable exception) {
            Log.e("Battery", "Error", exception);
        }*/
    }

    @Override
    public void onDestroy() {
        // Cancel the persistent notification.
        mNM.cancel(NOTIFICATION);

        ContextWrapper wrapper = new ContextWrapper(getBaseContext());

        screenController.getReceiver().stop(wrapper);
        powerController.getReceiver().stop(wrapper);
        volumeController.getReceiver().stop(wrapper);

        Intent i = new Intent(CustomIntent.SERVICE);
        i.putExtra(ServiceManager.SERVICE_STATE, ServiceState.Stopped.value);
        wrapper.sendBroadcast(i);

        // Tell the user we stopped.
        Toast.makeText(this, R.string.local_service_stopped, Toast.LENGTH_SHORT).show();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private void registerPowerReceiver(ContextWrapper wrapper, BroadcastReceiver receiver) {

        for (String actionName : powerIntents) {
            this.registerReceiver(receiver, new IntentFilter(actionName));
        }
    }

    /**
     * Show a notification while this service is running.
     */
    private void showNotification() {
/*
        // In this sample, we'll use the same text for the ticker and the expanded notification
        CharSequence text = getText(R.string.local_service_started);


// Set the icon, scrolling text and timestamp
        Notification notification = new Notification.Builder(getApplicationContext())
                .setContentTitle("New mail from ")
                .setContentText("subject")
                .setSmallIcon(R.drawable.ic_launcher)                 .build();

// The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 1,
                new Intent(this, MainActivity.class), 1);

// Set the info for the views that show in the notification panel.
        notification.setLatestEventInfo(this, "asdas",
                text, contentIntent);


        mNM.notify(R.string.local_service_started, notification);*/
    }

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class LocalBinder extends Binder {
        MainService getService() {
            return MainService.this;
        }
    }


}
